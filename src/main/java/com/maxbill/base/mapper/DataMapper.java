package com.maxbill.base.mapper;

import com.maxbill.base.bean.Connect;
import com.sun.mail.imap.protocol.ID;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 数据操作层
 *
 * @author MaxBill
 * @date 2019/07/06
 */
@Repository
public interface DataMapper {

    /**
     * 查询表是否存在
     *
     * @param tableName 表名
     * @return 0/1
     */
    @Select("SELECT COUNT(T.TABLENAME) FROM SYS.SYSTABLES T, SYS.SYSSCHEMAS S WHERE S.SCHEMANAME = 'APP' AND S.SCHEMAID = T.SCHEMAID AND T.TABLENAME=#{tableName}")
    int isExistsTable(@Param("tableName") String tableName);

    /**
     * 创建连接表
     */
    @Update("CREATE TABLE T_CONNECT (ID VARCHAR(50),TEXT VARCHAR(50),RHOST VARCHAR(50),RPORT VARCHAR(10),RPASS VARCHAR(50),SNAME VARCHAR(50),SHOST VARCHAR(50),SPORT VARCHAR(10),SPASS VARCHAR(50),SPKEY VARCHAR(500),ISHA VARCHAR(1),TYPE VARCHAR(1),TIME VARCHAR(50),PRIMARY KEY(ID))")
    void createConnectTable();

    /**
     * 创建设置表
     */
    @Update("CREATE TABLE T_SETTING(ID VARCHAR(50),PROP_SET VARCHAR(100),PROP_KEY VARCHAR(100),PROP_VAL VARCHAR(500),PRIMARY KEY(ID))")
    void createSettingTable();

    /**
     * 查询连接列表
     *
     * @return 连接名称列表
     */
    @Select("SELECT * FROM T_CONNECT")
    List<Connect> selectConnectList();

    /**
     * 插入连接数据
     *
     * @param connect 连接数据
     */
    @Insert("INSERT INTO T_CONNECT(ID, TEXT, RHOST, RPORT, RPASS, SNAME, SHOST, SPORT, SPASS, SPKEY, ISHA, TYPE, TIME) VALUES(#{o.id},#{o.text},#{o.rhost},#{o.rport},#{o.rpass},#{o.sname},#{o.shost},#{o.sport},#{o.spass},#{o.spkey},#{o.isha},#{o.type},#{o.time})")
    void insertConnect(@Param("o") Connect connect);

}


