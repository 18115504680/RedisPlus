package com.maxbill.fxui.body;

import javafx.scene.control.TabPane;
import javafx.scene.layout.Pane;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

import static com.maxbill.fxui.util.CommonConstant.*;

/**
 * 中心窗口
 *
 * @author MaxBill
 * @date 2019/07/06
 */
@Log4j2
public class BodyWindow {

    private static TabPane tabPane;

    static TabPane getTabPane() {
        return tabPane;
    }

    public static Pane getBodyWindow() {
        Pane bodyWindow = new Pane();
        bodyWindow.setId("body-pane");
        bodyWindow.getStylesheets().add(STYLE_BODY);
        bodyWindow.getStylesheets().add(STYLE_INFO);
        bodyWindow.getStylesheets().add(STYLE_WARN);
        bodyWindow.getStylesheets().add(STYLE_LOGS);
        bodyWindow.getStylesheets().add(STYLE_SBAR);
        tabPane = new TabPane();
        tabPane.setId("tabs-pane");
        tabPane.prefWidthProperty().bind(bodyWindow.widthProperty());
        tabPane.prefHeightProperty().bind(bodyWindow.heightProperty());
        bodyWindow.getChildren().add(tabPane);
        return bodyWindow;
    }

}
